<?php

namespace Drupal\Tests\real_aes\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group real_aes
 */
class RealAesInstallTest extends KernelTestBase {

  private const MODULE_NAME = 'real_aes';

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);

    // Try installing and uninstalling again.
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
  }

}
