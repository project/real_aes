<?php

namespace Drupal\real_aes\Plugin\EncryptionMethod;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Encoding;
use Defuse\Crypto\Exception\CryptoException;
use Defuse\Crypto\Key;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\encrypt\Attribute\EncryptionMethod;
use Drupal\encrypt\EncryptionMethodInterface;
use Drupal\encrypt\Exception\EncryptException;
use Drupal\encrypt\Plugin\EncryptionMethod\EncryptionMethodBase;
use Drupal\encrypt\Plugin\EncryptionMethodPluginFormInterface;
use Drupal\key\Entity\Key as DrupalKey;

/**
 * Provides an encryption method for using AES encryption.
 *
 * @EncryptionMethod(
 *   id = "real_aes",
 *   title = @Translation("Authenticated AES (Real AES)"),
 *   description = "Authenticated encryption based on AES-256 in CBC mode.",
 *   key_type = {"encryption"},
 *   can_decrypt = TRUE
 * )
 */
#[EncryptionMethod(
  id: "real_aes",
  title: new TranslatableMarkup("Authenticated AES (Real AES)"),
  description: new TranslatableMarkup("Authenticated encryption based on AES-256 in CBC mode."),
  key_type: ["encryption"],
  can_decrypt: TRUE,
)]
class RealAESEncryptionMethod extends EncryptionMethodBase implements EncryptionMethodInterface, EncryptionMethodPluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function checkDependencies($text = NULL, $key = NULL) {
    $errors = [];

    if (!class_exists('\Defuse\Crypto\Crypto')) {
      $errors[] = $this->t('Defuse PHP Encryption library is not correctly installed.');
    }

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function encrypt($text, $key, $options = []) {
    try {
      $key = Encoding::saveBytesToChecksummedAsciiSafeString(Key::KEY_CURRENT_VERSION, $key);
      // Convert the key to a Defuse Crypto key object.
      $key = Key::loadFromAsciiSafeString($key);
      return Crypto::encrypt((string) $text, $key);
    }
    catch (CryptoException $ex) {
      throw new EncryptException('Encryption error: ' . $ex->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($text, $key, $options = []) {
    try {
      $key = Encoding::saveBytesToChecksummedAsciiSafeString(Key::KEY_CURRENT_VERSION, $key);
      // Convert the key to a Defuse PHP-Encryption key object.
      $key = Key::loadFromAsciiSafeString($key);
      return Crypto::decrypt((string) $text, $key);
    }
    catch (CryptoException $ex) {
      throw new EncryptException('Decryption error: ' . $ex->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Validate the key size.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $key = DrupalKey::load($form_state->getUserInput()['encryption_key']);
    $keySizeBits = $key->getKeyType()->getConfiguration()['key_size'];
    if ($keySizeBits !== (Key::KEY_BYTE_SIZE * 8)) {
      $form_state->setErrorByName('encryption_key', $this->t('Incorrect encryption key size. AES encryption requires a 256-bit key.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
